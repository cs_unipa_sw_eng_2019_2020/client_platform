package com.ingsw.cfim.client_mvc.models;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class RestaurantTest {

    Restaurant restaurant;

    @Before
    public void setUp() {
        this.restaurant = new Restaurant();
        this.restaurant.setName("Foo");
        this.restaurant.setDescription("A cousine like a foobar");
        this.restaurant.setCuisine("Pizza gourmet");
        this.restaurant.setAddress("Via Michele cipolla 73, Palermo, 90123");
        Map<String, List<Product>> products = new HashMap<>();
        List<Product> prodotti = new ArrayList<>();
        Product prodotto = new Product();
        prodotto.setCategory("Antipasti");
        prodotto.setName("Patatine Fritte piccole");
        prodotti.add(prodotto);
        prodotto.setCategory("Antipasti");
        prodotto.setName("Patatine Fritte grandi");
        prodotti.add(prodotto);
        products.put("Antipasti", prodotti);
        this.restaurant.setProducts(products);
        // this.restaurant.setProducts("Antipasti", prodotti);
    }

    @Test
    public void nameIsSet() {
        assertNotEquals(this.restaurant.getName(), "");
        assertTrue(this.restaurant.getName() == "Foo");
    }

    @Test
    public void descriptionIsSet() {
        assertNotEquals(this.restaurant.description, "");
    }

    @Test
    public void addressIsSet() {
        assertNotEquals(this.restaurant.address, "");
    }

    @Test
    public void cuisineIsSet() {
        assertNotEquals(this.restaurant.cuisine, "");
    }

    @Test
    public void thereIsAnyProducts() {
        assertNotEquals(this.restaurant.products.size(), 0);
    }

}