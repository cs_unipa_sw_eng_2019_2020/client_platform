package com.ingsw.cfim.client_mvc.models;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.jupiter.api.Assertions.*;

public class ProductTest {

    Product product;

    @Before
    public void setUp() {
        this.product = new Product();
        this.product.setName("Pizza Margherita");
        this.product.setCategory("Pizza");
        this.product.setId("h28187121");
        this.product.setIngredients("Pomodoro, Mozzarella Santa Lucia, Origano");
        this.product.setPrice(4.50);
    }

    @Test
    public void isNameSet() {
        assertNotEquals(this.product.getName(), "");
    }

    @Test
    public void isCategorySet() {
        assertNotEquals(this.product.getCategory(), "");
    }

    @Test
    public void isIngredientsSet() {
        assertNotEquals(this.product.getIngredients(), "");
    }

    @Test
    public void isIdSet() {
        assertEquals(this.product.getId(), "h28187121");
    }

    @Test
    public void isPriceSet() {
        assertEquals(this.product.getPrice(), 4.50);
    }
}