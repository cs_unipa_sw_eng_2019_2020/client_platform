package com.ingsw.cfim.client_mvc.geo;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class GetCoordinatesFromAddressTest {

  @Autowired
  GeocodeRequest requestProvider;

  @Test
  public void gettingJSONCoordinates() {
    GeoJsonPoint coordinates = requestProvider.getAddressCoordinatesFromAPI("Viale+del+Fante+52,+Altofonte");
    assertNotNull(coordinates);
  }

  @Test
  public void gettingJSONCoordinatesMustReturnNull() {
    GeoJsonPoint coordinates = requestProvider.getAddressCoordinatesFromAPI(null);
    assertNull(coordinates);
  }

  @Test
  public void gettingJSONCoordinatesMustReturnEmptyAddressObject() throws JSONException, JsonProcessingException {
    GeoJsonPoint coordinates = requestProvider.getAddressCoordinatesFromAPI("asl3209qdwlqsasaa");
    assertNull(coordinates);
  }

}
