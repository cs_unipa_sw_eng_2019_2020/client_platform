package com.ingsw.cfim.client_mvc.services;

import com.ingsw.cfim.client_mvc.models.Restaurant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestaurantApiServiceTest {

    @Autowired
    RestaurantApiService restaurantService;

    /*
     * @Before public void setUp() { this.restaurantService = new
     * RestaurantApiService(); }
     */

    /**
     * TODO: Resolve troubles with API fetch
     */
    @Test
    public void getNullListOfRestaurants() {
        try {
            List<Restaurant> restaurants = this.restaurantService.getNearestRestaurants(20.0, 20.0, 10000.0);
            System.out.println("Number of restaurants: " + restaurants.size());
            assertThat(restaurants.size(), is(0));
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        // List<Restaurant> restaurants =
        // this.restaurantApiService.getNearestRestaurants(20.0, 20.0, 10000.0);
        // assertThat(restaurants.size(), is(0));
    }

    /**
     * TODO: Check why getListOfRestaurants return
     */

    @Test
    public void getListOfRestaurants() {
        List<Restaurant> restaurants = this.restaurantService.getNearestRestaurants(13.29319, 38.07665, 10930.0);
        System.out.println("Number of restaurants: " + restaurants.size());
        // assertThat(restaurants.size(), is(not(0)));
        assertNotEquals(restaurants.size(), 0);
    }

}