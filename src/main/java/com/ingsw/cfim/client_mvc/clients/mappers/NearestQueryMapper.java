package com.ingsw.cfim.client_mvc.clients.mappers;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NearestQueryMapper {
  private Double lat;
  private Double lng;
  private Double dist;
}