package com.ingsw.cfim.client_mvc.clients;

import com.ingsw.cfim.client_mvc.clients.mappers.NearestQueryMapper;
import com.ingsw.cfim.client_mvc.models.ProductsMap;
import com.ingsw.cfim.client_mvc.models.Restaurant;

import org.bson.types.ObjectId;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;

@FeignClient(name = "restaurantsClient", url = "http://localhost:8080/api/v1/restaurants")
public interface RestaurantsClient {
  @GetMapping("/")
  List<Restaurant> nearest(@SpringQueryMap NearestQueryMapper params);

  @GetMapping("/{id}")
  Restaurant getRestaurant(@PathVariable("id") ObjectId id);

  @GetMapping("/{id}/products")
  ProductsMap restaurantProducts(@PathVariable("id") ObjectId id);

}
