package com.ingsw.cfim.client_mvc.forms;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AddressForm {
    @NotBlank
    String address;

    public void setAddress(String address) {
        String[] addressArray = address.split(" ");
        this.address = String.join("+", addressArray);
    }

    public String friendlyViewAddress(String address) {
        return address.replace("+", " ");
    }
}
