package com.ingsw.cfim.client_mvc.geo;

import com.ingsw.cfim.client_mvc.geo.builder.Geocode;
import com.ingsw.cfim.client_mvc.geo.builder.GeocodeRequestBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;

@Configuration
@PropertySource("classpath:geo.properties")
public class GeoServiceConfiguration {

  @Value("${geo.endpoint}")
  private String endpoint;

  @Value("${geo.apiKey}")
  private String apiKey;

  @Bean
  public String geoEndpoint() {
    return this.endpoint;
  }

  @Bean
  public String apiKey() {
    return this.apiKey;
  }

  @Bean
  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  public Geocode geocodeBuilder() {
    return new GeocodeRequestBuilder();
  }

  @Bean
  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  public GeocodeRequest geoRequest() {
    return new GeocodeRequest(this.geocodeBuilder(), this.geoEndpoint(), this.apiKey());
  }

}
