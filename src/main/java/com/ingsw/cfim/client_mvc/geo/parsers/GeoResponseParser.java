package com.ingsw.cfim.client_mvc.geo.parsers;

import org.json.JSONObject;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

public interface GeoResponseParser {
  public JSONObject partial = new JSONObject();

  public GeoResponseParser parseLocation();

  public GeoJsonPoint getParsedCoordinates();
}
