package com.ingsw.cfim.client_mvc.geo.parsers;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

public final class AddressParser implements GeoResponseParser {
  private JSONObject partial;
  private GeoJsonPoint parsedLocation;

  public AddressParser(JSONObject json) {
    JSONArray temp = json.getJSONObject("Response").getJSONArray("View");

    if(temp.isEmpty()) {
      this.partial = null;
    } else {
      this.partial = temp.getJSONObject(0).getJSONArray("Result")
          .getJSONObject(0).getJSONObject("Location");
    }
  }

  @Override
  public AddressParser parseLocation() {
    if(this.partial != null) {
      this.partial = this.partial.getJSONObject("DisplayPosition");

      double longitude = this.partial.getDouble("Longitude");
      double latitude = this.partial.getDouble("Latitude");
      this.parsedLocation = new GeoJsonPoint(longitude, latitude);
    }

    return this;
  }

  @Override
  public GeoJsonPoint getParsedCoordinates() {
    return this.parsedLocation;
  }

}
