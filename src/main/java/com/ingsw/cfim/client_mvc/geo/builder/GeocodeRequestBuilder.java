package com.ingsw.cfim.client_mvc.geo.builder;

import org.springframework.web.util.UriComponentsBuilder;

public class GeocodeRequestBuilder implements Geocode{

  private UriComponentsBuilder uri;

  @Override
  public Geocode setAPIEndpoint(String url) {
    this.uri = UriComponentsBuilder.fromUriString(url);
    return this;
  }

  @Override
  public Geocode setRequestParam(String key, String value) {
    this.uri.queryParam(key, value);
    return this;
  }

  @Override
  public String buildRequestUrl() {
    return this.uri.toUriString();
  }
}
