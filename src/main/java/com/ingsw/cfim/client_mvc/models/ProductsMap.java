package com.ingsw.cfim.client_mvc.models;

import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class ProductsMap {
  Map<String, List<Product>> products;
}
