package com.ingsw.cfim.client_mvc.models;

import java.util.Set;
import java.util.List;
import java.util.Map;

// import org.bson.types.ObjectId;

import lombok.Data;

@Data
public class Restaurant {
  String id;
  String name;
  String description;
  String cuisine;
  Set<String> categories;
  String type;
  String vatNumber;
  String phoneNumber;
  String address;
  String openingStatus;

  Map<String, List<Product>> products;

  public String firstToUpper(String s) {
    return s.substring(0, 1).toUpperCase() + s.substring(1);
  }

}
