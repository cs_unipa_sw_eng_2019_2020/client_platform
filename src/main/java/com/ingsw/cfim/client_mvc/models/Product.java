package com.ingsw.cfim.client_mvc.models;

import lombok.Data;

@Data
public class Product {
  String id;
  String name;
  String category;
  String ingredients;
  Double price;
}
